import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PrefixerProcessor implements Processor {
    private static final Logger LOGGER = LoggerFactory.getLogger(PrefixerProcessor.class);

    public void process(Exchange exchange) {
        String inBody = exchange.getIn().getBody(String.class);
        LOGGER.info("Received IN message with body {}", inBody);
        LOGGER.info("Prefixing body...");
        inBody = "Prefixed " + inBody;
        exchange.getIn().setBody(inBody);
    }
}
